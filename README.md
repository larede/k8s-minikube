# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* testar serviços core KOI

### How do I get set up? ###

* minikube start --insecure-registry=registry.airc.pt
* eval $(minikube docker-env)
* docker login registry.airc.pt
* docker pull registry.airc.pt/postgres:12.2-AIRC
* kubectl apply -f postgres.yaml
* kubectl apply -f pgadmin.yaml
* kubectl get pods
* minikube ip pgadmin
* goto browser [ip]:30200
* minikube delete

### TO-DO ###

* keycloak
* alfresco
* tnt
* adm
* instalar receita no ibm cloud